%define major 3

Name:    freeimage
Version: 3.18.0
Release: 14
Summary: FreeImage is a library project for developers who would like to support popular graphics image formats (PNG, JPEG, TIFF, BMP and others)
License: GPL-2.0-only OR GPL-3.0-only and FreeImage
URL:	 https://freeimage.sourceforge.io/
Source0: http://downloads.sourceforge.net/freeimage/FreeImage3180.zip

Patch0:         CVE-2019-12211_2019-12213.patch

# https://github.com/glennrp/libpng/commit/32784523239c376dfe544bb6c7012c4934624a6d
Patch1:         fix-libpng-arm-neon.patch
Patch2:         FreeImage_unbundle.patch
# Fix incorrect path in doxyfile
Patch3:         FreeImage_doxygen.patch
# Fix incorrect variable names in BIGENDIAN blocks
Patch4:         FreeImage_bigendian.patch
Patch5:		substream.patch
Patch6:		Fix-build-failure-with-OpenEXR-3.0.patch
Patch7:         freeimage-libtiff45.patch
Patch8:         Fix-build-failure-with-LibRaw-0.21.1.patch 
# https://sources.debian.org/src/freeimage/3.18.0%2Bds2-10/debian/patches/
Patch9:         CVE-2020-21427-pre-r1830-minor-refactoring.patch
Patch10:        CVE-2020-21427-1-r1832-improved-BMP-plugin-when-working-with-malicious-images.patch
Patch11:        CVE-2020-21428-r1877-improved-DDS-plugin-against-malicious-images.patch
Patch12:        CVE-2020-21427-2-r1836-improved-BMP-plugin-when-working-with-malicious-images.patch
Patch13:        CVE-2020-22524-r1848-improved-PFM-plugin-against-malicious-images.patch
# https://src.fedoraproject.org/rpms/freeimage/tree/f39
Patch14:        CVE-2020-24292.patch
Patch15:        CVE-2020-24293.patch
Patch16:        CVE-2020-24295.patch
Patch17:        CVE-2021-33367.patch
Patch18:        CVE-2021-40263.patch
Patch19:        CVE-2021-40266.patch
Patch20:        CVE-2023-47995.patch
Patch21:        CVE-2023-47997.patch

BuildRequires:  doxygen  gcc-c++ make jxrlib-devel libjpeg-devel libmng-devel libpng-devel libtiff-devel libwebp-devel LibRaw-devel OpenEXR-devel openjpeg2-devel


%description
FreeImage is a library project for developers who would like to support popular graphics image formats (PNG, JPEG, TIFF, BMP and others). Some highlights are: extremely simple in use, not limited to the local PC (unique FreeImageIO) and Plugin driven!

%package devel
Summary: FreeImage is a library project for developers who would like to support popular graphics image formats (PNG, JPEG, TIFF, BMP and others)
Requires: %{name} = %{version}-%{release}
%description devel
FreeImage is a library project for developers who would like to support popular graphics image formats (PNG, JPEG, TIFF, BMP and others). Some highlights are: extremely simple in use, not limited to the local PC (unique FreeImageIO) and Plugin driven!

%prep
%autosetup -p1 -n FreeImage 
# remove all included libs to make sure these don't get used during compile
rm -r Source/Lib* Source/ZLib Source/OpenEXR

# clear files which cannot be built due to dependencies on private headers
# (see also unbundle patch)
> Source/FreeImage/PluginG3.cpp
> Source/FreeImageToolkit/JPEGTransform.cpp

# sanitize encodings / line endings
for file in `find . -type f -name '*.c' -or -name '*.cpp' -or -name '*.h' -or -name '*.txt' -or -name Makefile`; do
  iconv --from=ISO-8859-15 --to=UTF-8 $file > $file.new && \
  sed -i 's|\r||g' $file.new && \
  touch -r $file $file.new && mv $file.new $file
done

%build
sh ./gensrclist.sh
sh ./genfipsrclist.sh
%ifarch %{power64} %{mips32} aarch64
%make_build -f Makefile.gnu CFLAGS="%{optflags} -fPIC" CXXFLAGS="%{optflags} -fPIC" LDFLAGS="%{__global_ldflags}"
%make_build -f Makefile.fip CFLAGS="%{optflags} -fPIC" CXXFLAGS="%{optflags} -fPIC" LDFLAGS="%{__global_ldflags}"
%else
%make_build -f Makefile.gnu CFLAGS="%{optflags}" CXXFLAGS="%{optflags}" LDFLAGS="%{__global_ldflags}"
%make_build -f Makefile.fip CFLAGS="%{optflags}" CXXFLAGS="%{optflags}" LDFLAGS="%{__global_ldflags}"
%endif

pushd Wrapper/FreeImagePlus/doc
doxygen FreeImagePlus.dox
popd


%install
install -Dpm 755 Dist/lib%{name}-%{version}.so %{buildroot}%{_libdir}/lib%{name}-%{version}.so
ln -s lib%{name}-%{version}.so %{buildroot}%{_libdir}/lib%{name}.so

install -Dpm 755 Dist/lib%{name}plus-%{version}.so %{buildroot}%{_libdir}/lib%{name}plus-%{version}.so
ln -s lib%{name}plus-%{version}.so %{buildroot}%{_libdir}/lib%{name}plus.so
install -Dpm 644 Source/FreeImage.h %{buildroot}%{_includedir}/FreeImage.h
install -Dpm 644 Wrapper/FreeImagePlus/FreeImagePlus.h %{buildroot}%{_includedir}/FreeImagePlus.h

%files
%license license-*.txt 
%doc README.linux README.md Whatsnew.txt
%{_libdir}/lib%{name}-%{version}.so
%{_libdir}/lib%{name}.so.*
%{_libdir}/lib%{name}plus-%{version}.so
%{_libdir}/lib%{name}plus.so.*

%files devel
%doc Examples Wrapper/FreeImagePlus/html
%{_includedir}/FreeImage.h
%{_libdir}/lib%{name}.so
%{_includedir}/FreeImagePlus.h
%{_libdir}/lib%{name}plus.so

%changelog
* Sun Oct 27 2024 Funda Wang <fundawang@yeah.net> - 3.18.0-14
- cleanup spec
- finally fix linkage

* Wed Oct 23 2024 wangkai <13474090681@163.com> - 3.18.0-13
- Fix CVE-2020-24292 CVE-2020-24293 CVE-2020-24295 CVE-2021-33367
  CVE-2021-40263 CVE-2021-40266 CVE-2023-47995 CVE-2023-47997

* Mon Aug 19 2024 xu_ping <707078654@qq.com> - 3.18.0-12
- License compliance rectification.

* Mon Dec 04 2023 wangkai <13474090681@163.com> - 3.18.0-11
- Fix CVE-2020-21427,CVE-2020-21428,CVE-2020-22524

* Wed Jul 5 2023 liyanan <thistleslyn@163.com> - 3.18.0-10
- Fix compilation failure caused by LibRaw upgrade 

* Mon Feb 13 2023 wulei <wulei80@h-partners.com> - 3.18.0-9
- Add patch for libtiff-4.5.0 comptability

* Wed Aug 24 2022 caodongxia <caodongxia@h-partners.com> -3.18.0-8
- Add debug package to add strip

* Tue Mar 01 2022 weidong <weidong@uniontech.com> -3.18.0-7
- Fix build error

* Tue Feb 09 2021 Jiachen Fan <fanjiachen3@huawei.com> -3.18.0-6
- correct the substream.patch: correct the line

* Wed Dec 16 2020 Senlin <xiasenlin1@huawei.com> -3.18.0-5
- correct the substream.patch: add an empty line at end of file

* Tue Dec 15 2020 Senlin <xiasenlin1@huawei.com> -3.18.0-4
- Rebuild for new LibRaw

* Mon Nov 09 2020 weidong <weidong@uniontech.com>
- Unbundle bundled libraries
- Fix incorrect path in doxyfile
- Fix incorrect variable names in BIGENDIAN blocks

* Thu Sep 10 2020 weidong <weidong@uniontech.com>
- fix libpng arm neon in freeimage 

* Mon May 4 2020 Wei Xiong <myeuler@163.com>
- Package init

